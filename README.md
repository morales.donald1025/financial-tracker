<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profit And Expenses Tracker</title>
    <link href="main.css" rel="stylesheet">
</head>

<body>
<header class="container">
    <div class="nav">
    <nav>
        <img src="/images/logo.png">
       <h2><a href="main.html">mathweb.com</a></h2>
    </nav>
<!--Log In-->



  
  
  
  </div>

</div>
<div class="app-container">
    <div class="main-container">
        <div id="btn-main-profit">
            <h2><a href="/profitComponents/profit.html">Profit Tracker</a></h2>
        </div>
        
        <div id="btn-main-expenses">
            <h2><a href="/expensesComponents/expenses.html"> Expenses Tracker</a></h2>
        </div>

    </div>
    </div>
</header>
<footer class="foot">
    <h4 class="powered">Powered by: mathweb.com</h4>
    <h4 class="copy">&copy; 2021 Donald Morales</h4>
</footer>    
</body>
<script src="main.js"></script>
</html>